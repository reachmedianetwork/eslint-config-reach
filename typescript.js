module.exports = {
    overrides: [
        {
            files: [ '*.ts', '*.tsx', '*.vue' ], // Apply TypeScript rules to these filetypes
            extends: [
                'plugin:@typescript-eslint/eslint-recommended',
                'plugin:@typescript-eslint/recommended',
                './rules/idiomatic/typescript',
                './rules/reach/typescript',
            ],
            plugins: [
                '@typescript-eslint',
            ],
            parser: '@typescript-eslint/parser',
        },
    ],
};
