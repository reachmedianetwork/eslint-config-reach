# @reach/eslint-config

> REACH Media Network ESLint rules

**⚠️ This has been superseded by [eslint-config-reach-prettier].**

[eslint-config-reach-prettier]: https://bitbucket.org/reachmedianetwork/eslint-config-reach-prettier

---

This package contains all of our ESLint rules.

## Installation

Install the package and all of its dependencies:

```sh
npm install --save-dev bitbucket:reachmedianetwork/eslint-config-reach.git#semver:^0

# dependencies
npm install --save-dev eslint
# the following is only needed if your project contains TypeScript
npm install --save-dev @typescript-eslint/parser @typescript-eslint/eslint-plugin
```

Afterwards, create a `.eslintrc.js` file in your root project folder and add the
following:

```javascript
module.exports = {
    extends: [
        '@reach/eslint-config',
        // the following is only needed if your project contains TypeScript
        '@reach/eslint-config/typescript',
    ],
    ignorePatterns: [
        'dist/*',
    ],
    overrides: [
        {
            files: [
                '.eslintrc.js',
            ],
            env: {
                node: true,
            },
        },
    ],
}
```

Add the following scripts to `package.json`:

```json
{
    // ...
    "scripts": {
        // ...
        "lint": "npm run lint:js",
        "lint:js": "eslint --max-warnings 0 \"**/*.{js,ts}\"",
        "lint:js:fix": "eslint --fix \"**/*.{js,ts}\"",
        // ...
    }
    // ...
}
```
