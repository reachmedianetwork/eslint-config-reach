module.exports = {
    extends: [
        'eslint:recommended',
        './rules/idiomatic',
        './rules/reach',
    ],
};
