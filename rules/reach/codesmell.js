module.exports = {
    rules: {
        // code smell
        'complexity': [ 'error' ],
        'max-depth': [ 'warn' ],
        'max-lines': [ 'warn', {
            'max': 500,
            'skipBlankLines': true,
            'skipComments': true,
        } ],
        'max-lines-per-function': [ 'warn', {
            'max': 200,
            'skipBlankLines': true,
            'skipComments': true,
        } ],
        'max-nested-callbacks': [ 'warn' ],
        'max-statements': [ 'warn', {
            'max': 20,
        } ],
    },
};
