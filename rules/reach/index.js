module.exports = {
    rules: {
        'array-callback-return': [ 'error' ],
        'array-bracket-newline': [ 'error', 'consistent' ],
        'array-element-newline': [ 'error', 'consistent' ],
        'comma-dangle': [ 'error', 'always-multiline' ],
        'semi': [ 'error', 'always' ],
        'computed-property-spacing': [ 'error', 'never' ],
        'func-names': [ 'off' ],
        'function-call-argument-newline': [ 'error', 'consistent' ],
        'function-paren-newline': [ 'error', 'multiline-arguments' ],
        'indent': [ 'error', 4, {
            'SwitchCase': 1,
        } ],
        'key-spacing': [ 'error', {
            'beforeColon': false,
            'afterColon': true,
            'mode': 'minimum',
        } ],
        'linebreak-style': [ 'off' ],
        'newline-per-chained-call': [ 'off' ],
        'no-inline-comments': [ 'off' ],
        'object-curly-newline': [ 'error', {
            'consistent': true,
        } ],
        'object-property-newline': [ 'error', {
            'allowAllPropertiesOnSameLine': true,
        } ],
        'one-var': [ 'error', 'never' ],
        'prefer-promise-reject-errors': [ 'warn', {
            'allowEmptyReject': false,
        } ],
        'quotes': [ 'error', 'single', {
            'avoidEscape': true,
        } ],
        'vars-on-top': [ 'off' ],

        // max line length
        'max-len': [ 'warn', {
            'code': 120,
            'comments': 80,
            'ignoreTrailingComments': true,
            'ignoreUrls': true,
            'ignoreStrings': true,
            'ignoreTemplateLiterals': true,
            'ignoreRegExpLiterals': true,
            'ignorePattern': 'module:',
        } ],
    },
};
