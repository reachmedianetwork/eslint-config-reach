/*
Extension Rules (Extends ESLint rule to support Typescript syntax)
{@link https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/README.md#extension-rules}

@typescript-eslint/brace-style
@typescript-eslint/comma-spacing
@typescript-eslint/default-param-last
@typescript-eslint/dot-notation
@typescript-eslint/func-call-spacing
@typescript-eslint/indent
@typescript-eslint/init-declarations
@typescript-eslint/keyword-spacing
@typescript-eslint/no-array-constructor
@typescript-eslint/no-dupe-class-members
@typescript-eslint/no-empty-function
@typescript-eslint/no-extra-parens
@typescript-eslint/no-extra-semi
@typescript-eslint/no-invalid-this
@typescript-eslint/no-magic-numbers
@typescript-eslint/no-unused-expressions
@typescript-eslint/no-unused-vars
@typescript-eslint/no-use-before-define
@typescript-eslint/no-useless-constructor
@typescript-eslint/quotes
@typescript-eslint/require-await
@typescript-eslint/return-await
@typescript-eslint/semi
@typescript-eslint/space-before-function-paren
*/

module.exports = {
    rules: {
        'indent': 'off',
        '@typescript-eslint/indent': [ 'error', 4, { 'SwitchCase': 1 } ],

        'quotes': 'off',
        '@typescript-eslint/quotes': [ 'error', 'single', { 'avoidEscape': true } ],
    },
};
