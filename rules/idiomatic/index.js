/**
 * @file Rules pulled from [jamespamplin/eslint-config-idiomatic]{@link https://github.com/jamespamplin/eslint-config-idiomatic}
 * @copyright 2015 James Pamplin
 * @license MIT
 * @version 4.0.0
 */

module.exports = {
    rules: {
        'array-bracket-spacing': [ 'error', 'always' ],
        'arrow-spacing': [ 'error', { 'before': true, 'after': true } ],
        'block-spacing': [ 'error', 'always' ],
        'brace-style': [ 'error', '1tbs', { 'allowSingleLine': false } ],
        'comma-spacing': [ 'error', { 'before': false, 'after': true } ],
        'comma-style': [ 'error', 'last' ],
        'computed-property-spacing': [ 'error', 'always' ],
        'curly': [ 'error', 'all' ],
        'dot-location': [ 'error', 'property' ],
        'eol-last': [ 'error' ],
        'func-names': [ 'warn' ],
        'indent': [ 'error', 2 ],
        'key-spacing': [ 'error', { 'beforeColon': false, 'afterColon': true } ],
        'keyword-spacing': [ 'error' ],
        'linebreak-style': [ 'error' ],
        'no-eq-null': [ 'off' ],
        'no-func-assign': [ 'error' ],
        'no-inline-comments': [ 'error' ],
        'no-mixed-spaces-and-tabs': [ 'error' ],
        'no-multi-spaces': [ 'error' ],
        'no-spaced-func': [ 'error' ],
        'no-trailing-spaces': [ 'error' ],
        'object-curly-spacing': [ 'error', 'always' ],
        'one-var': [ 'error', 'always' ],
        'one-var-declaration-per-line': [ 'error', 'always' ],
        'quotes': [ 'error', 'single' ],
        'semi-spacing': [ 'error', { 'before': false, 'after': true } ],
        'space-before-blocks': [ 'error' ],
        'space-before-function-paren': [ 'error', 'never' ],
        'space-in-parens': [ 'error', 'always', { 'exceptions': [ '{}', '[]', '()', 'empty' ] } ],
        'space-infix-ops': [ 'error' ],
        'space-unary-ops': [ 'error', { 'words': true, 'nonwords': false } ],
        'vars-on-top': [ 'error' ],
    },
};
